from django.shortcuts import render
from django.http import HttpResponse 
from .models import Libro

# Create your views here.

def index(request):
    return render(request,'index.html',{'Nombre':"Patricio", 'elementos':Libro.objects.all()})

def registro(request):
    return render(request,'formulario.html',{})

def crear(request):
    Nombre = request.POST.get('Nombre',' ')
    Autor = request.POST.get('Autor',' ')
    Año = request.POST.get('Año',0)
    libro = Libro(Nombre=Nombre,Autor=Autor,Año=Año)
    libro.save()
    
    return HttpResponse(' Nombre:  ' +Nombre+ '  Autor:   ' +Autor+ ' Año: ' +Año)

def buscar(request,id):
    libro = Libro.objects.get(pk=id)
    return HttpResponse(' Nombre: ' +libro.Nombre+ '  Autor:   '+libro.Autor)

def editar(request,id):
    libro = Libro.objects.get(pk=id)
    return render(request,'editar.html',{'libro':libro})

def eliminar(request,id):
    libro = Libro.objects.get(pk=id)
    libro.delete()
    return HttpResponse("Libro Eliminado")
    
def editado(request,id):
    libro = Libro.objects.get(pk=id)
    Nombre = request.POST.get('Nombre',' ')
    Autor = request.POST.get('Autor',' ')
    Año = request.POST.get('Año',0)
    libro.Nombre = Nombre
    libro.Autor = Autor
    libro.Año = Año
    libro.save()

    return HttpResponse("Nombre:  "+libro.Nombre+"  Autor: "+libro.Autor+"  Año: "+libro.Año)
